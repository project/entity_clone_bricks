<?php

namespace Drupal\entity_clone_bricks\EntityClone\Content;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\entity_clone\EntityClone\Content\ContentEntityCloneBase;

/**
 * Class BricksEntityCloneBase.
 */
class BricksEntityCloneBase extends ContentEntityCloneBase {

  /**
   * {@inheritdoc}
   */
  protected function fieldIsClonable(FieldDefinitionInterface $field_definition) {
    $is_cloneable = parent::fieldIsClonable($field_definition);
    $cloneable_field_types = [
      'bricks',
    ];

    $type_is_cloneable = in_array($field_definition->getType(), $cloneable_field_types, TRUE);
    if (($field_definition instanceof FieldConfigInterface) && $type_is_cloneable) {
      $is_cloneable = TRUE;
    }
    return $is_cloneable;
  }

}
