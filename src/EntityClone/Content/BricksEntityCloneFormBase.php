<?php

namespace Drupal\entity_clone_bricks\EntityClone\Content;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\entity_clone\EntityClone\Content\ContentEntityCloneFormBase;

/**
 * Class BricksEntityCloneFormBase.
 */
class BricksEntityCloneFormBase extends ContentEntityCloneFormBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(EntityInterface $entity, $parent = TRUE, &$discovered_entities = []) {
    $form = parent::formElement($entity, $parent, $discovered_entities);

    if ($entity instanceof FieldableEntityInterface) {
      $discovered_entities[$entity->getEntityTypeId()][$entity->id()] = $entity;
      foreach ($entity->getFieldDefinitions() as $field_id => $field_definition) {
        if ($field_definition instanceof FieldConfigInterface && in_array($field_definition->getType(), ['bricks'], TRUE)) {
          $field = $entity->get($field_id);
          /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $value */
          if ($field->count() > 0) {
            $form['recursive'] = array_merge($form['recursive'], $this->getRecursiveFormElement($field_definition, $field_id, $field, $discovered_entities));
          }
        }
      }
    }

    return $form;
  }

}
